ó
I«Wc           @   s    d  d l  Z  d  d l m Z d  d l m Z d  d l m Z m Z d  d l m	 Z	 d  d l
 m Z d Z d Z d	 Z d
 Z d e e f d     YZ d   Z d S(   iÿÿÿÿN(   t   abstractmethod(   t   Node(   t   ValidationErrort   MissingPluginError(   t   LinkType(   t$   SealableWithUpdatableAttributesMixinu   Copyright (c), This file is part of the AiiDA platform. For further information please visit http://www.aiida.net/. All rights reserved.s"   MIT license, see LICENSE.txt file.s   0.7.0s   The AiiDA team.t   AbstractCodec           B   sy  e  Z d  Z d   Z d   Z d   Z d   Z d   Z d   Z e	 e
 d  d  d    Z e	 e
 d     Z e	 e
 e d	    Z d
   Z d  e j d  Z d   Z d   Z d   Z d   Z d   Z d   Z d   Z d   Z d   Z e
 d    Z d   Z d   Z e
 d    Z  d   Z! d   Z" e
 d    Z# d   Z$ d   Z% e& d    Z' e	 d    Z( RS(!   s  
    A code entity.
    It can either be 'local', or 'remote'.

    * Local code: it is a collection of files/dirs (added using the add_path() method), where one     file is flagged as executable (using the set_local_executable() method).

    * Remote code: it is a pair (remotecomputer, remotepath_of_executable) set using the     set_remote_computer_exec() method.

    For both codes, one can set some code to be executed right before or right after
    the execution of the code, using the set_preexec_code() and set_postexec_code()
    methods (e.g., the set_preexec_code() can be used to load specific modules required
    for the code to be run).
    c         C   s   d |  _  d	 g |  _ d S(
   s<   
        This function is called by the init method
        t   input_plugint   append_textt   prepend_textt   hiddent   remote_computer_exect   local_executableN(   s   input_plugins   append_texts   prepend_texts   hidden(   s   remote_computer_execs   local_executable(   t   _updatable_attributest   _set_incompatibilities(   t   self(    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   _init_internal_params    s    	c         C   s   |  j  d t  d S(   sQ   
        Hide the code (prevents from showing it in the verdi code list)
        R
   N(   t	   _set_attrt   True(   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   _hide*   s    c         C   s   |  j  d t  d S(   so   
        Reveal the code (allows to show it in the verdi code list)
        By default, it is revealed
        R
   N(   R   t   False(   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   _reveal0   s    c         C   s   |  j  d t  S(   s>   
        Determines whether the Code is hidden or not
        R
   (   t   get_attrR   (   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt
   _is_hidden7   s    c         C   sP   t  | t  r | g } n  x. | D]& } |  j | t j j |  d  q" Wd S(   sJ  
        Given a list of filenames (or a single filename string),
        add it to the path (all at level zero, i.e. without folders).
        Therefore, be careful for files with the same name!

        :todo: decide whether to check if the Code must be a local executable
             to be able to call this function.
        i   N(   t
   isinstancet
   basestringt   add_patht   ost   patht   split(   R   t   filest   f(    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt	   set_files=   s    
c         C   sy   |  j    r d n d } |  j    r- d } n* |  j   d  k	 rQ |  j   j } n d } d j | |  j | |  j |  j  S(   Nt   Localt   Remotet
   repositorys	   [unknown]s$   {} code '{}' on {}, pk: {}, uuid: {}(   t   is_localt   get_computert   Nonet   namet   formatt   labelt   pkt   uuid(   R   t	   local_strt   computer_str(    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   __str__L   s    		c         C   s   d S(   së  
        Get a code from its label.

        :param label: the code label
        :param computername: filter only codes on computers with this name
        :param useremail: filter only codes belonging to a user with this
          email

        :raise NotExistent: if no matches are found
        :raise MultipleObjectsError: if multiple matches are found. In this case
          you may want to pass the additional parameters to filter the codes,
          or relabel the codes.
        N(    (   t   clsR)   t   computernamet	   useremail(    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   get[   s    c         C   s  d d l  m } m } d d l m } yf t |  } y | | d |  SWn? | k
 re t    n& | k
 r | d j |    n XWnt k
 r| j d  \ } } } | rÕ |  j	 d | d |  }	 n |  j	 d |  }	 t
 |	  d	 k r| d
 j |    qt
 |	  d k rd j |  }
 |
 d j t g  |	 D] } t | j  ^ qE  d 7}
 |
 d 7}
 | |
   q|	 d	 Sn Xd S(   s  
        Get a Computer object with given identifier string, that can either be
        the numeric ID (pk), or the label (if unique); the label can either
        be simply the label, or in the format label@machinename. See the note
        below for details on the string detection algorithm.

        .. note:: If a string that can be converted to an integer is given,
          the numeric ID is verified first (therefore, is a code A with a
          label equal to the ID of another code B is present, code A cannot
          be referenced by label). Similarly, the (leftmost) '@' symbol is
          always used to split code and computername. Therefore do not use
          '@' in the code name if you want to use this function
          ('@' in the computer name are instead valid).

        :param code_string: the code string identifying the code to load

        :raise NotExistent: if no code identified by the given string is found
        :raise MultipleObjectsError: if the string cannot identify uniquely
            a code
        iÿÿÿÿ(   t   NotExistentt   MultipleObjectsError(   t	   load_nodet   parent_classs*   More than one code in the DB with pk='{}'!t   @R)   t   dbcomputer__namei    s%   '{}' is not a valid code ID or label.i   s6   There are multiple codes with label '{}', having IDs: s   , s   .
s>   Relabel them (using their ID), or refer to them with their ID.N(   t   aiida.common.exceptionsR3   R4   t   aiida.orm.utilsR5   t   intt
   ValueErrorR(   t	   partitiont   queryt   lent   joint   sortedt   strR*   (   R/   t   code_stringR3   R4   R5   t   code_intt   codenamet   sepR0   t   codest   retstrt   c(    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   get_from_stringm   s4    			9
c         C   s   d S(   sp  
        Return a list of valid code strings for a given plugin.

        :param plugin: The string of the plugin.
        :param labels: if True, return a list of code names, otherwise
          return the code PKs (integers).
        :return: a list of string, with the code names if labels is True,
          otherwise a list of integers with the code PKs.
        N(    (   R/   t   plugint   labels(    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   list_for_pluginª   s    c         C   sé   t  t |   j   |  j   d  k r4 t d   n  |  j   r |  j   s[ t d   n  |  j   |  j   k rå t d j |  j      qå nQ |  j   r¯ t d   n  |  j	   sÊ t d   n  |  j
   så t d   n  d  S(   Ns3   You did not set whether the code is local or remotesW   You have to set which file is the local executable using the set_exec_filename() methodsB   The local executable '{}' is not in the list of files of this codes*   The code is remote but it has files insides%   You did not specify a remote computers'   You did not specify a remote executable(   t   superR   t	   _validateR$   R&   R   t   get_local_executablet   get_folder_listR(   t   get_remote_computert   get_remote_exec_path(   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyRO   ¸   s$    c         C   s   t  d   d  S(   Ns'   A code node cannot have any input nodes(   R<   (   R   t   srcR)   t	   link_type(    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   add_link_fromÓ   s    c         C   sG   d d l  m } t | |  s. t d   n  t t |   j | |  S(   s   
        Raise a ValueError if a link from self to dest is not allowed.

        An output of a code can only be a calculation
        iÿÿÿÿ(   t   Calculations3   The output of a code node can only be a calculation(   t   aiida.orm.calculationRW   R   R<   RN   R   t   _linking_as_output(   R   t   destRU   RW   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyRY   Ö   s
    c         C   s   |  j  d t |   d S(   sz   
        Pass a string of code that will be put in the scheduler script before the
        execution of the code.
        R	   N(   R   t   unicode(   R   t   code(    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   set_prepend_textä   s    c         C   s   |  j  d d  S(   s   
        Return the code that will be put in the scheduler script before the
        execution, or an empty string if no pre-exec code was defined.
        R	   u    (   R   (   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   get_prepend_textë   s    c         C   s9   | d k r |  j d d  n |  j d t |   d S(   s   
        Set the name of the default input plugin, to be used for the automatic
        generation of a new calculation.
        R   N(   R&   R   R[   (   R   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   set_input_plugin_nameò   s    c         C   s   |  j  d d  S(   sj   
        Return the name of the default input plugin (or None if no input plugin
        was set.
        R   N(   R   R&   (   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   get_input_plugin_nameü   s    c         C   s   |  j  d t |   d S(   sy   
        Pass a string of code that will be put in the scheduler script after the
        execution of the code.
        R   N(   R   R[   (   R   R\   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   set_append_text  s    c         C   s   |  j  d d  S(   s`   
        Return the postexec_code, or an empty string if no post-exec code was defined.
        R   u    (   R   (   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   get_append_text
  s    c         C   s   |  j    |  j d |  d S(   se   
        Set the filename of the local executable.
        Implicitly set the code as local.
        R   N(   t
   _set_localR   (   R   t	   exec_name(    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   set_local_executable  s    
c         C   s   |  j  d d  S(   NR   u    (   R   (   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyRP     s    c         C   s   d S(   sÇ  
        Set the code as remote, and pass the computer on which it resides
        and the absolute path on that computer.

        Args:
            remote_computer_exec: a tuple (computer, remote_exec_path), where
              computer is a aiida.orm.Computer or an
              aiida.backends.djsite.db.models.DbComputer object, and
              remote_exec_path is the absolute path of the main executable on
              remote computer.
        N(    (   R   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   set_remote_computer_exec  s    c         C   s+   |  j    r t d   n  |  j d d  S(   Ns   The code is localt   remote_exec_patht    (   R$   R<   R   (   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyRS   *  s    c         C   s%   |  j    r t d   n  |  j   S(   Ns   The code is local(   R$   R<   R%   (   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyRR   /  s    c         C   s   d S(   s  
        Set the code as a 'local' code, meaning that all the files belonging to the code
        will be copied to the cluster, and the file set with set_exec_filename will be
        run.

        It also deletes the flags related to the local case (if any)
        N(    (   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyRc   5  s    	c         C   s9   |  j  d t  y |  j d  Wn t k
 r4 n Xd S(   s'  
        Set the code as a 'remote' code, meaning that the code itself has no files attached,
        but only a location on a remote computer (with an absolute path of the executable on
        the remote computer).

        It also deletes the flags related to the local case (if any)
        R$   R   N(   R   R   t	   _del_attrt   AttributeError(   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   _set_remote@  s
    c         C   s   |  j  d d  S(   s   
        Return True if the code is 'local', False if it is 'remote' (see also documentation
        of the set_local and set_remote functions).
        R$   N(   R   R&   (   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyR$   N  s    c         C   s   d S(   s$  
        Return True if this code can run on the given computer, False otherwise.

        Local codes can run on any machine; remote codes can run only on the machine
        on which they reside.

        TODO: add filters to mask the remote machines on which a local code can run.
        N(    (   R   t   computer(    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt
   can_run_onU  s    
c         C   s-   |  j    r d j |  j    S|  j   Sd S(   sÃ   
        Return the executable string to be put in the script.
        For local codes, it is ./LOCAL_EXECUTABLE_NAME
        For remote codes, it is the absolute path to the executable.
        u   ./{}N(   R$   R(   RP   RS   (   R   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   get_execnamea  s    c         O   s¾   d d l  m } |  j   } | d k r7 t d   n  y | |  } Wn& t k
 ro t d j |    n X|  j   s d | k r |  j   | d <q n  | | |   } | j	 |   | S(   s  
        Create and return a new Calculation object (unstored) with the correct
        plugin subclass, as obtained by the self.get_input_plugin_name() method.

        Parameters are passed to the calculation __init__ method.

        :note: it also directly creates the link to this code (that will of
            course be cached, since the new node is not stored yet).

        :raise MissingPluginError: if the specified plugin does not exist.
        :raise ValueError: if no plugin was specified.
        iÿÿÿÿ(   t   CalculationFactorys1   You did not specify an input plugin for this codesQ   The input_plugin name for this code is '{}', but it is not an existing pluginnameRl   N(
   R:   Ro   R`   R&   R<   R   R(   R$   RR   t   use_code(   R   t   argst   kwargsRo   t   plugin_namet   Ct   new_calc(    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyRu   l  s    	c         C   sq  g  } | j  d j |  j   | j  d j |  j   | j  d j |  j   | j  d j |  j   | j  d j |  j     | j  d j t |  j      |  j	   rA| j  d j d   | j  d	 j |  j
     | j  d
  x¤ |  j j   D]7 } | j  d j |  j j |  r-d n d |   qWnY | j  d j d   | j  d j |  j   j   | j  d  | j  d |  j    | j  d  |  j   j   ròxC |  j   j d  D] } | j  d j |   qÏWn | j  d  | j  d  |  j   j   rWxC |  j   j d  D] } | j  d j |   q4Wn | j  d  d j |  S(   sr   
        Return a (multiline) string with a human-readable detailed information
        on this computer.
        s    * PK:             {}s    * UUID:           {}s    * Label:          {}s    * Description:    {}s    * Default plugin: {}s"    * Used by:        {} calculationss    * Type:           {}t   locals    * Exec name:      {}s    * List of files/folders:s      * [{}] {}s    dirt   filet   remotes    * Remote machine: {}s    * Remote absolute path: s      s    * prepend text:s   
s      {}s      # No prepend text.s    * append text:s      # No append text.(   t   appendR(   R*   R+   R)   t   descriptionR`   R?   t   get_outputsR$   Rn   t   _get_folder_pathsubfoldert   get_content_listt   isdirRR   R'   RS   R^   t   stripR   Rb   R@   (   R   t	   ret_linest   fnamet   l(    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   full_text_info  sF    	c         K   sb   d d l  m } |   j |  } y | j   Wn+ t k
 r] } t d j | j    n X| S(   Niÿÿÿÿ(   t   CodeInputValidationClasss!   Unable to store the computer: {}.(   t   aiida.cmdline.commands.codeR   t   set_and_validate_from_codet   storeR   R(   t   message(   R/   Rr   R   R\   t   e(    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   setupÂ  s    N()   t   __name__t
   __module__t   __doc__R   R   R   R   R    R.   t   classmethodR    R&   R2   RJ   R   RM   RO   R   t   UNSPECIFIEDRV   RY   R]   R^   R_   R`   Ra   Rb   Re   RP   Rf   RS   RR   Rc   Rk   R$   Rm   Rn   Ru   t   propertyR   R   (    (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyR      sJ   	
					<					
											&0c         C   s
   t   d S(   s]  
    Delete a code from the DB.
    Check before that there are no output nodes.

    NOTE! Not thread safe... Do not use with many users accessing the DB
    at the same time.

    Implemented as a function on purpose, otherwise complicated logic would be
    needed to set the internal state of the object after calling
    computer.delete().
    N(   t   NotImplementedError(   R\   (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   delete_codeÐ  s    (   R   t   abcR    t   aiida.orm.implementationR   R9   R   R   t   aiida.common.linksR   t   aiida.orm.mixinsR   t   __copyright__t   __license__t   __version__t   __authors__R   R   (    (    (    sJ   /Users/pxu/codes/AiiDA/aiida_epfl/aiida/orm/implementation/general/code.pyt   <module>   s   ÿ Â