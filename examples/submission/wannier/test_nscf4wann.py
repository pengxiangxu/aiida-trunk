#!/usr/bin/env runaiida
# -*- coding: utf-8 -*-
import sys
import os

__authors__ = "The AiiDA team."
__copyright__ = u"Copyright (c), This file is part of the AiiDA platform. For further information please visit http://www.aiida.net/. All rights reserved"
__license__ = "Non-Commercial, End-User Software License Agreement, see LICENSE.txt file."
__version__ = "0.7.0"

################################################################
UpfData = DataFactory('upf')
ParameterData = DataFactory('parameter')
KpointsData = DataFactory('array.kpoints')
StructureData = DataFactory('structure')
RemoteData = DataFactory('remote')

# Used to test the parent calculation
QEPwCalc = CalculationFactory('quantumespresso.pw') 

try:
    dontsend = sys.argv[1]
    if dontsend == "--dont-send":
        submit_test = True
    elif dontsend == "--send":
        submit_test = False
    else:
        raise IndexError
except IndexError:
    print >> sys.stderr, ("The first parameter can only be either "
                          "--send or --dont-send")
    sys.exit(1)

try:
    parent_id = sys.argv[2]
except IndexError:
    print >> sys.stderr, ("Must provide as second parameter the parent ID")
    sys.exit(1)


#####
# test parent

try:
    int(parent_id)
except ValueError:
    raise ValueError('Parent_id not an integer: {}'.format(parent_id))

parentcalc = Calculation.get_subclass_from_pk(parent_id)

queue = None
#queue = "P_share_queue"

#####

if isinstance(parentcalc,QEPwCalc):

    calc = parentcalc.create_restart(use_output_structure=True)
    calc.label = "Test QE pw.x nscf calculation"
    calc.description = "Test nscf calculation with the Quantum ESPRESSO pw.x code"

else:
    print >> sys.stderr, ("Parent calculation should be a pw.x "
                          "calculation.")
    sys.exit(1)

######
try:
    settings_dict = calc.inp.settings.get_dict()
except AttributeError:
    settings_dict = {}
settings_dict.update({ 'FORCE_KPOINTS_LIST':True})

new_input_dict = calc.inp.parameters.get_dict()


new_input_dict['CONTROL'].update({'calculation': 'nscf'})
new_input_dict['SYSTEM'].update({'nosym': True})
# new_input_dict['SYSTEM'].update({'nbnd':20}) # Tune if you need more bands



kpoints = KpointsData()
kpoints_mesh = 4
kpoints.set_kpoints_mesh([kpoints_mesh, kpoints_mesh, kpoints_mesh])

calc.use_kpoints(kpoints)
calc.use_parameters(ParameterData(dict=new_input_dict))
calc.use_settings(ParameterData(dict=settings_dict))

if submit_test:
    subfolder, script_filename = calc.submit_test()
    print "Test_submit for calculation (uuid='{}')".format(
        calc.uuid)
    print "Submit file in {}".format(os.path.join(
        os.path.relpath(subfolder.abspath),
        script_filename
        ))
else:
    calc.store_all()
    print "created calculation; calc=Calculation(uuid='{}') # ID: {}".format(
        calc.uuid,calc.dbnode.pk)
    calc.submit()
    print "submitted calculation; calc=Calculation(uuid='{}') # ID: {}".format(
        calc.uuid,calc.dbnode.pk)

