#!/usr/bin/env runaiida
# -*- coding: utf-8 -*-
import sys
import os
from aiida.orm import DataFactory, CalculationFactory
from aiida.common.example_helpers import test_and_get_code

__authors__ = "The AiiDA team."
__copyright__ = u"Copyright (c), This file is part of the AiiDA platform. For further information please visit http://www.aiida.net/. All rights reserved"
__license__ = "Non-Commercial, End-User Software License Agreement, see LICENSE.txt file."
__version__ = "0.7.0"

ParameterData = DataFactory('parameter')
StructureData = DataFactory('structure')
KpointsData = DataFactory('array.kpoints')

try:
    send_param = sys.argv[1]
    if send_param == "--dont-send":
        submit_test = True
    elif send_param == "--send":
        submit_test = False
    else:
        raise IndexError
except IndexError:
    print >> sys.stderr, ("The first parameter can only be either "
                          "--send or --dont-send")
    sys.exit(1)

try:
    parent_id = sys.argv[2]
except IndexError:
    print >> sys.stderr, ("Must provide as second parameter the parent ID")
    sys.exit(1)

try:
    codename = sys.argv[3]
except IndexError:
    print >> sys.stderr, ("Must provide as third parameter the main code")
    sys.exit(1)

try:
    pre_codename = sys.argv[4]
except IndexError:
    print >> sys.stderr, ("Must provide as fourth parameter the precode, use"
                          " 'None' to avoid using pre_codename")
    sys.exit(1)

code = test_and_get_code(codename, expected_code_type='wannier90')

if pre_codename != 'None':
    pre_code = test_and_get_code(pre_codename, expected_code_type='wannier90')
else:
    pre_code = None

parent_calc = Calculation.get_subclass_from_pk(parent_id)



###############SETTING UP WANNIER PARAMETERS ###################################

exclude_bands = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
parameter = ParameterData(dict={'bands_plot':True,
                                'num_iter': 100,
                                'dis_num_iter': 200,
                                'num_print_cycles': 10,
                                'guiding_centres': True,
                                'num_wann': 9,
                                'exclude_bands': exclude_bands,
                                # 'wannier_plot':True,
                                # 'wannier_plot_list':[1]
                                })

structure = parent_calc.get_inputs_dict()['structure']

kpoints = KpointsData()
kpoints = parent_calc.get_inputs_dict()['kpoints']

kpoints_path = KpointsData()
kpoints_path.set_cell_from_structure(structure)
kpoints_path.set_kpoints_path()

calc = code.new_calc()
calc.set_max_wallclock_seconds(30*60) # 30 min
calc.set_resources({"num_machines": 1})

precode_parameter = {}
# precode_parameter.update({'write_unk':True})
precode_parameter = ParameterData(dict=precode_parameter)

if pre_code:
    calc.use_preprocessing_code(pre_code)
    calc.use_precode_parameters(precode_parameter)
calc.use_parent_calculation(parent_calc)
calc.use_structure(structure)

orbitaldata = calc.gen_projections([{'kind_name':"O",'ang_mtm_name':"P"}])
calc.use_projections(orbitaldata)

# calc.use_projections(orbitaldata)
calc.use_parameters(parameter)
calc.use_kpoints(kpoints)
calc.use_kpoints_path(kpoints_path)


# settings that can only be enabled if parent is nscf
settings_dict = {}
# settings_dict.update({'INIT_ONLY':True}) # for setup calculation

if settings_dict:
    settings = ParameterData(dict=settings_dict)
    calc.use_settings(settings)

if submit_test:
    subfolder, script_filename = calc.submit_test()
    print "Test_submit for calculation (uuid='{}')".format(
        calc.uuid)
    print "Submit file in {}".format(os.path.join(
        os.path.relpath(subfolder.abspath),
        script_filename
    ))
else:
    calc.store_all()
    print "created calculation; calc=Calculation(uuid='{}') # ID={}".format(
        calc.uuid, calc.dbnode.pk)
    calc.submit()
    print "submitted calculation; calc=Calculation(uuid='{}') # ID={}".format(
        calc.uuid, calc.dbnode.pk)
