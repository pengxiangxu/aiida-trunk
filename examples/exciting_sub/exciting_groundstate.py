#from aiida.backends.utils import load_dbenv
#load_dbenv()

from math import *

num_kp = 1

num_nodes = num_kp * 1
num_ranks_per_node = 8
num_threads_per_rank = 1

num_ranks = num_nodes * num_ranks_per_node
#n = int(sqrt(num_ranks / num_kp))
#num_diag = n * n

job_tag = str(num_nodes) + "N:" + str(num_ranks_per_node) + "R:" + str(num_threads_per_rank) + "T"

print "number of nodes            : ", num_nodes
print "number of ranks per node   : ", num_ranks_per_node
print "total number of ranks      : ", num_ranks
print "number of threads per rank : ", num_threads_per_rank
print "job_tag is ", job_tag

from aiida.orm import Code

#code = Code.get(label='exciting', computername='daint', useremail='xup@phys.ethz.ch')
codename = 'exciting@daint'
code = Code.get_from_string(codename)
from aiida.orm import load_node
s = load_node(192)

from aiida.orm.data.parameter import ParameterData
parameters = ParameterData(dict={'groundstate' : {'xctype' : 'LDA_PZ', 'gmaxvr' : '12.0'}})


#settings = ParameterData(dict={})
#
from aiida.orm import DataFactory
KpointsData = DataFactory('array.kpoints')
kpoints = KpointsData()
kpoints.set_kpoints_mesh([4, 4, 4], offset=(0.0, 0.0, 0.0))
#
calc = code.new_calc()
calc.set_max_wallclock_seconds(60*60)
calc.set_resources({"num_machines": num_nodes, "num_mpiprocs_per_machine": num_ranks_per_node})

calc.use_structure(s)
calc.use_parameters(parameters)
calc.use_kpoints(kpoints)
#
#calc.use_settings(settings)
#
calc.use_lapwbasis_from_family('high_quality_lapw_species')
#
calc.set_environment_variables({'OMP_NUM_THREADS': str(num_threads_per_rank)})
calc.set_mpirun_extra_params(['-c', str(num_threads_per_rank), '-n', str(num_ranks_per_node)])
calc.set_custom_scheduler_commands("#SBATCH --constraint=gpu")
#
calc.label = "lapw groundstate"
calc.description = job_tag
#
calc.store_all()
#
#grp, created = Group.get_or_create(name='Au-surf_QE_CPU_5-Nov-2015')
#grp.add_nodes([calc])
#
print "created calculation; with uuid='{}' and PK={}".format(calc.uuid, calc.pk)

calc.submit()
#calc.submit_test()



